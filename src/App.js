import React from 'react';

import Header from './components/Header';
import Hero from './components/Hero';
import News from './components/News';
import About from './components/About';
import Download from './components/Download';
import Footer from './components/Footer';

import './App.css';

function App() {
  return (
    <div className="App">
      <Header />
      <Hero />
      <News />
      <About />
      <Download />
      <Footer />
    </div>
  );
}

export default App;
